import { initialTask } from "../../utils/utils";
import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { IInitialTask } from "../../types/allTypes";
import { nanoid } from "nanoid";

// Define a type for the slice state
interface taskState {
  value: number;
  taskList: IInitialTask[];
  selectTask: IInitialTask;
}

// Define the initial state using that type
const initialState: taskState = {
  value: 0,
  taskList: [],
  selectTask: initialTask,
};

export const taskSlice = createSlice({
  name: "task",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    changeInput: (state, action) => {
      let { value } = action.payload;
      state.selectTask = { ...state.selectTask, taskName: value };

      return { ...state };
    },
    addTask: (state, action) => {
      if (!action.payload.taskName) {
        alert("Task name is required!");
        return { ...state };
      }
      const today = new Date(),
        currentTime = `${today.getHours()}:${today.getMinutes()}`;
      const _id = nanoid();
      const dtoTask = {
        _id,
        taskName: action.payload?.taskName,
        done: false,
        pinned: false,
        deadlineDate: new Date(),
        backgroundColor: action.payload?.backgroundColor,
        textColor: action.payload?.textColor,
      };

      const taskListUpdate = [...state.taskList, dtoTask];
      const index = state.taskList.findIndex(
        (task) => task.taskName === action.payload
      );

      if (index !== -1) {
        alert("This task already existed!!");
      } else {
        return { ...state, taskList: taskListUpdate };
      }
    },
    doneTask: (state, action) => {
      let taskListUpdate = [...state.taskList];
      let index = state.taskList.findIndex(
        (task) => task._id === action.payload
      );

      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return void { ...state, taskList: taskListUpdate };
    },
    deleteTask: (state, action) => {
      let taskListUpdate = [...state.taskList];
      let updatedList = taskListUpdate.filter(
        (task) => task._id !== action.payload
      );

      return { ...state, taskList: updatedList };
    },
    editTask: (state, action) => {
      return { ...state, selectTask: action.payload };
    },
    updateTask: (state, action) => {
      let taskListUpdate = [...state.taskList];
      let updatedList = taskListUpdate.filter(
        (task) => task._id !== action.payload._id
      );
      let updateTask = action.payload;
      let data = [updateTask, ...updatedList];

      return { ...state, taskList: data, selectTask: initialTask };
    },
    pinnedTask: (state, action) => {
      let taskListUpdate = [...state.taskList];
      let index = state.taskList.findIndex(
        (task) => task._id === action.payload
      );

      if (index !== -1) {
        taskListUpdate[index].pinned = true;
      }

      return void { ...state, taskList: taskListUpdate };
    },
    unPinnedTask: (state, action) => {
      let taskListUpdate = [...state.taskList];
      let index = state.taskList.findIndex(
        (task) => task._id === action.payload
      );

      if (index !== -1) {
        taskListUpdate[index].pinned = false;
      }

      return void { ...state, taskList: taskListUpdate };
    },
    increment: (state) => {
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
  },
});

export const {
  increment,
  decrement,
  incrementByAmount,
  addTask,
  changeInput,
  deleteTask,
  doneTask,
  editTask,
  pinnedTask,
  unPinnedTask,
  updateTask,
} = taskSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectCount = (state: RootState) => state.taskSlice.value;
export const selectedTask = (state: RootState) => state.taskSlice.selectTask;
export const taskList = (state: RootState) => state.taskSlice.taskList;

export default taskSlice.reducer;
