import axios from "axios";

const BASE_URL = "https://uptask-api.vercel.app/";

export const https = axios.create({
  baseURL: BASE_URL,
  headers: {
    accept: "application/json",
  },
});
