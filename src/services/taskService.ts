import { IAddTaskRequestBody, IUpdateTaskRequestBody } from "../types/allTypes";
import { https } from "./configURL";

export const taskService = {
  addTask: async (requestBody: IAddTaskRequestBody) => {
    let response = await https.post("/api/tasks/create", requestBody);
    return response.data;
  },
  getAllTasks: async () => {
    let response = await https.get("/api/tasks");
    return response.data;
  },
  deleteSingleTask: async (taskId: string) => {
    let response = await https.delete(`/api/tasks/${taskId}`);
    return response.data;
  },
  pinTask: async (taskId: string) => {
    let response = await https.put(`/api/tasks/${taskId}/pinned?pinned=true`);
    return response.data;
  },
  unpinTask: async (taskId: string) => {
    let response = await https.put(`/api/tasks/${taskId}/pinned?pinned=false`);
    return response.data;
  },
  doneTask: async (taskId: string) => {
    let response = await https.put(`/api/tasks/${taskId}/done?done=true`);
    return response.data;
  },
  undoneTask: async (taskId: string) => {
    let response = await https.put(`/api/tasks/${taskId}/done?done=false`);
    return response.data;
  },
  updateTask: async ({ taskId, requestBody }: IUpdateTaskRequestBody) => {
    let response = await https.put(`/api/tasks/${taskId}/update`, requestBody);
    return response.data;
  },
};
