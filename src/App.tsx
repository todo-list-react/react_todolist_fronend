import React, { useState } from "react";
import "./App.css";
import { AppContext } from "./context/AppContext";
import { TodoListLayout } from "./component/Todolist/layout";
import MainPlaceComponent from "./component/Todolist/MainPlaceComponent";
import { QueryClient, QueryClientProvider } from "react-query";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const queryClient = new QueryClient();

function App() {
  const [isDarkMode, setIsDarkMode] = useState<boolean>(
    localStorage.getItem("DARK-MODE")
      ? localStorage.getItem("DARK-MODE") === "dark"
      : false
  );
  return (
    <div className={isDarkMode ? "dark" : ""}>
      <QueryClientProvider client={queryClient}>
        <AppContext.Provider value={{ isDarkMode, setIsDarkMode }}>
          <TodoListLayout>
            <MainPlaceComponent />
            <ToastContainer theme="dark" />
          </TodoListLayout>
        </AppContext.Provider>
      </QueryClientProvider>
    </div>
  );
}

export default App;
