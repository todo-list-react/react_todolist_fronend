import { useQuery } from "react-query";
import { taskService } from "../services/taskService";

export const useTaskData = () => {
  return useQuery(["taskData"], () => taskService.getAllTasks());
};
