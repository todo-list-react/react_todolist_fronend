export interface IInitialTask {
  _id: string;
  taskName: string;
  done: boolean;
  pinned: boolean;
  deadlineDate: Date;
  backgroundColor: string;
  textColor: string;
}

export interface IAddTaskRequestBody {
  taskName: string;
  done: boolean;
  pinned: boolean;
  deadlineDate: Date;
  backgroundColor: string;
  textColor: string;
}

export interface IUpdateTaskRequestBody {
  taskId: string;
  requestBody: {
    taskName: string;
    done: boolean;
    pinned: boolean;
    deadlineDate: Date;
    backgroundColor: string;
    textColor: string;
  };
}
