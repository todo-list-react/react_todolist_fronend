import { faTasks, faXmarkCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { toast } from "react-toastify";
import { IAddTaskRequestBody } from "../../types/allTypes";
import { useMutation, useQueryClient } from "react-query";
import { taskService } from "../../services/taskService";
import { randomColorList } from "../../utils/randomColorList";

type Inputs = {
  taskName: string;
  done: boolean;
  pinned: boolean;
  deadlineDate: Date;
  backgroundColor: string;
  textColor: string;
};

interface Props {
  handleCloseAddTaskForm: () => void;
}

function AddTaskForm({ handleCloseAddTaskForm }: Props) {
  const randomColor = randomColorList[Math.floor(Math.random() * 5)];
  const [addedTaskData, setAddedTaskData] = useState<IAddTaskRequestBody>({
    taskName: "",
    done: false,
    pinned: false,
    deadlineDate: new Date(),
    backgroundColor: randomColor.background,
    textColor: randomColor.text,
  });
  const queryClient = useQueryClient();
  const addTask = useMutation(
    (requestBody: IAddTaskRequestBody) => {
      return taskService.addTask(requestBody);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully deleted task from List", {
          position: toast.POSITION.TOP_CENTER,
        });
        handleCloseAddTaskForm();
      },
      onError: () => {
        toast.error("Failed to remove task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    if (data.taskName) {
      setAddedTaskData(data);
      const deadlineFormatted = new Date(data?.deadlineDate?.toString());
      const offsetMinutes = deadlineFormatted?.getTimezoneOffset();

      // Adjust the date by adding the offset
      deadlineFormatted?.setMinutes(
        deadlineFormatted?.getMinutes() - offsetMinutes
      );
      addTask.mutate({
        ...addedTaskData,
        deadlineDate: new Date(deadlineFormatted),
        taskName: data.taskName,
      });
    }
  };

  return (
    <div className="fixed bottom-0 left-0 right-0 top-0 z-20 bg-black/10">
      <div className="flex h-screen items-center justify-center">
        <div className="relative w-fit rounded-lg bg-white p-5 dark:bg-slate-800">
          <h2 className="border-b text-xl font-bold dark:text-white">
            Add Task
          </h2>
          <form onSubmit={handleSubmit(onSubmit)}>
            <FontAwesomeIcon
              onClick={handleCloseAddTaskForm}
              icon={faXmarkCircle}
              className="absolute -right-2 -top-2 cursor-pointer text-xl dark:text-white/60 dark:hover:text-white/80"
            />
            <div className="mx-auto my-3">
              <label
                className="mb-2 block font-bold text-yellow-500"
                htmlFor="taskName"
              >
                Task Name
              </label>
              <div className="relative flex items-center">
                <input
                  className="block w-full rounded-lg border-[0.5px] bg-transparent p-2 pr-10 font-medium text-gray-900 focus:border-black focus:ring-black dark:text-yellow-300 dark:outline-yellow-300 dark:ring-yellow-200"
                  type="text"
                  id="taskName"
                  autoFocus
                  {...register("taskName", {
                    required: {
                      value: true,
                      message: "Required",
                    },
                  })}
                  placeholder="Fill task name..."
                />
                <FontAwesomeIcon
                  icon={faTasks}
                  className="absolute right-2 h-5 w-5 text-black dark:text-white"
                />
              </div>
              <p className="w-full text-red-500 dark:text-red-400">
                {errors.taskName?.message}
              </p>
            </div>
            <div className="mx-auto my-3">
              <label
                className="mb-2 block font-bold text-yellow-500"
                htmlFor="deadlineDate"
              >
                Deadline (Date & Time)
              </label>
              <input
                className="block w-full rounded-lg border-[0.5px] bg-transparent p-2 font-medium text-gray-900 focus:border-black focus:ring-black dark:text-yellow-300 dark:outline-yellow-300 dark:ring-yellow-200"
                type="datetime-local"
                id="deadlineDate"
                defaultValue={new Date(addedTaskData?.deadlineDate)
                  ?.toISOString()
                  ?.slice(0, 16)}
                {...register("deadlineDate")}
                placeholder=""
              />
              <p className="w-full text-red-500 dark:text-red-400">
                {errors.deadlineDate?.message}
              </p>
            </div>

            <div className="flex flex-col items-start py-3">
              <button
                type="submit"
                className="mx-auto w-fit rounded border px-5 py-2 text-lg font-black shadow-xl transition-all hover:bg-white/50 hover:shadow-none disabled:cursor-not-allowed disabled:opacity-50 dark:text-white md:text-base"
              >
                Finish
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddTaskForm;
