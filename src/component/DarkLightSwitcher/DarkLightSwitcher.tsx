import React, { useContext, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCloudSun, faMoon } from "@fortawesome/free-solid-svg-icons";
import { AppContext } from "../../context/AppContext";

export function DarkLightSwitcher() {
  const { isDarkMode, setIsDarkMode } = useContext(AppContext);
  useEffect(() => {
    localStorage.setItem("DARK-MODE", isDarkMode ? "dark" : "light");
  }, [isDarkMode]);
  return (
    <button
      className="mr-2 flex h-7 w-7 items-center justify-center rounded-full border-2 border-slate-400 p-2 shadow-lg md:w-16 md:justify-normal "
      onClick={() => {
        if (setIsDarkMode) {
          setIsDarkMode(!isDarkMode);
        }
      }}
      title="Dark-Light Switcher"
    >
      <FontAwesomeIcon
        icon={isDarkMode ? faMoon : faCloudSun}
        className={`${isDarkMode ? "text-teal-500" : "text-yellow-500"} ${
          isDarkMode ? "md:translate-x-8" : "md:translate-x-0"
        } text-[1rem] transition-all duration-500`}
      />
    </button>
  );
}
