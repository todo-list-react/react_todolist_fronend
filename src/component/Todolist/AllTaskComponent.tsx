import {
  faArrowRotateBack,
  faCheckSquare,
  faStopwatch,
  faThumbTack,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import React, { useState } from "react";
import { useQueryClient, useMutation } from "react-query";
import { toast } from "react-toastify";
import { useTaskData } from "../../hooks/useTaskData";
import { taskService } from "../../services/taskService";
import { IInitialTask } from "../../types/allTypes";
import UpdateTaskForm from "../UpdateTaskForm/UpdateTaskForm";
import { RiUnpinFill } from "react-icons/ri";

function AllTaskComponent() {
  const { data: taskData } = useTaskData();
  const queryClient = useQueryClient();
  const [updateTaskFormState, setUpdateTaskFormState] = useState({
    isOpen: false,
    taskId: "",
  });

  const deleteTask = useMutation(
    (taskId: string) => {
      return taskService.deleteSingleTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully deleted task from List", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to remove task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const pinTask = useMutation(
    (taskId: string) => {
      return taskService.pinTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully pinned task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to pin task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const doneTask = useMutation(
    (taskId: string) => {
      return taskService.doneTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully complete task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to complete task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const undoneTask = useMutation(
    (taskId: string) => {
      return taskService.undoneTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully reverse task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to reverse task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const unpinTask = useMutation(
    (taskId: string) => {
      return taskService.unpinTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully unpin task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to unpin task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const renderTaskToDo = () => {
    return (taskData?.taskList as IInitialTask[])?.map((task, index) => {
      const handleOpenUpdateTaskForm = () => {
        setUpdateTaskFormState({
          ...updateTaskFormState,
          isOpen: true,
          taskId: task._id,
        });
      };

      const handleCloseUpdateTaskForm = () => {
        setUpdateTaskFormState({ ...updateTaskFormState, isOpen: false });
      };
      return (
        <div key={index.toString() + task._id}>
          {updateTaskFormState.isOpen &&
          updateTaskFormState.taskId === task._id ? (
            <UpdateTaskForm
              taskId={task._id}
              handleCloseUpdateTaskForm={handleCloseUpdateTaskForm}
            />
          ) : (
            <></>
          )}
          <div
            className={`${task.backgroundColor} ${task.textColor} w-fit rounded-xl p-4 dark:text-white dark:saturate-150`}
          >
            <div className="flex items-center justify-between gap-12">
              <div className="flex flex-wrap items-center gap-2">
                {task.done ? (
                  <FontAwesomeIcon icon={faCheckSquare} className="text-xl" />
                ) : (
                  <FontAwesomeIcon icon={faStopwatch} className="text-xl" />
                )}
                {moment(new Date(task?.deadlineDate)).format(
                  "DD/MM/YYYY hh:mm"
                )}
              </div>
              {task.done ? (
                <></>
              ) : (
                <button className="pinned_btn border-0">
                  {task.pinned ? (
                    <RiUnpinFill
                      onClick={() => {
                        unpinTask.mutate(task._id);
                      }}
                    />
                  ) : (
                    <FontAwesomeIcon
                      icon={faThumbTack}
                      onClick={() => {
                        pinTask.mutate(task._id);
                      }}
                    />
                  )}
                </button>
              )}
            </div>
            <div title={task.taskName} className="py-3 text-5xl">
              {task.taskName}
            </div>
            <div className="flex gap-2">
              {task.done ? (
                <button
                  onClick={() => {
                    undoneTask.mutate(task._id);
                  }}
                  className="ml-1 border-0"
                >
                  <FontAwesomeIcon icon={faArrowRotateBack} />
                </button>
              ) : (
                <>
                  <button
                    onClick={handleOpenUpdateTaskForm}
                    className="ml-1 border-0 "
                  >
                    <i className="fa-solid fa-edit"></i>
                  </button>
                  <button
                    onClick={() => {
                      doneTask.mutate(task._id);
                    }}
                    className="ml-1 border-0"
                  >
                    <i className="fa-solid fa-check"></i>
                  </button>
                </>
              )}
              <button
                onClick={() => {
                  deleteTask.mutate(task._id);
                }}
                className="ml-1 border-0"
              >
                <FontAwesomeIcon icon={faTrash} />
              </button>
            </div>
          </div>
        </div>
      );
    });
  };

  return (
    <div>
      <div className="flex flex-wrap gap-6">{renderTaskToDo()}</div>
    </div>
  );
}

export default AllTaskComponent;
