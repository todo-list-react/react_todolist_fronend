import React, { useState } from "react";
import TaskCompleteComponent from "./TaskCompleteComponent";
import TaskToDoComponent from "./TaskToDoComponent";
import AddTaskForm from "../AddTaskForm/AddTaskForm";
import AllTaskComponent from "./AllTaskComponent";
import { Tab } from "@headlessui/react";
import { nanoid } from "nanoid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";

export function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

export default function MainPlaceComponent() {
  const [state, setState] = useState({
    dateValue: new Date(),
    display: "flex",
  });
  const [isOpenAddTaskForm, setIsOpenAddTaskForm] = useState(false);

  const handleOpenAdTaskForm = () => {
    setIsOpenAddTaskForm(true);
  };

  const handleCloseAddTaskForm = () => {
    setIsOpenAddTaskForm(false);
  };

  const onRenderTabHead = () => {
    const tabHeadList = [
      { id: nanoid(), name: "Today's missions" },
      { id: nanoid(), name: "All" },
      { id: nanoid(), name: "Completed" },
    ];
    return tabHeadList?.map((item) => {
      return (
        <Tab
          key={item.id + item.name}
          className={({ selected }) =>
            classNames(
              "w-fit rounded-lg px-6 py-1 text-base font-semibold leading-5 transition-all duration-300",
              "outline-none ring-0",
              selected
                ? "border border-yellow-500 text-black opacity-90 hover:opacity-100 dark:border-yellow-300 dark:text-yellow-300"
                : "border border-black/80 text-black opacity-50 hover:opacity-100 dark:border-blue-300 dark:text-blue-300"
            )
          }
        >
          {item.name}
        </Tab>
      );
    });
  };

  const onRenderTabPanels = () => {
    const tabPanelList = [
      {
        id: nanoid(),
        component: <TaskToDoComponent dateValue={state.dateValue} />,
      },
      { id: nanoid(), component: <AllTaskComponent /> },
      {
        id: nanoid(),
        component: <TaskCompleteComponent />,
      },
    ];
    return tabPanelList?.map((item) => {
      return (
        <Tab.Panel
          key={item.id}
          className={classNames("pt-5", "outline-none ring-0")}
        >
          {item.component}
        </Tab.Panel>
      );
    });
  };

  return (
    <div className="min-h-screen dark:bg-gray-900">
      {isOpenAddTaskForm ? (
        <AddTaskForm handleCloseAddTaskForm={handleCloseAddTaskForm} />
      ) : (
        <></>
      )}
      <div className="pt-10">
        <div className="my-5 p-3 md:p-5">
          <h2 className="text-4xl font-semibold text-yellow-500 dark:text-yellow-300 md:text-[3rem]">
            Welcome to UPtask
          </h2>
          <div className="flex flex-col gap-3 pb-5 text-2xl md:text-3xl">
            <h4 className="dark:text-white">
              {state.dateValue.toDateString()}
            </h4>
            <div className="flex items-center gap-5 text-2xl">
              <FontAwesomeIcon
                onClick={() => {
                  const previousDay = new Date(state.dateValue.getTime());
                  previousDay.setDate(state.dateValue.getDate() - 1);

                  setState({ ...state, dateValue: previousDay });
                }}
                icon={faAngleLeft}
                className="flex h-6 w-6 cursor-pointer items-center justify-center rounded-full bg-slate-300/30 p-2 transition-colors duration-300 hover:bg-slate-200 dark:bg-white/80 dark:hover:bg-white"
              />

              <FontAwesomeIcon
                onClick={() => {
                  const previousDay = new Date(state.dateValue.getTime());
                  previousDay.setDate(state.dateValue.getDate() - 1);

                  setState({ ...state, dateValue: previousDay });
                }}
                icon={faAngleRight}
                className="flex h-6 w-6 cursor-pointer items-center justify-center rounded-full bg-slate-300/30 p-2 transition-colors duration-300 hover:bg-slate-200 dark:bg-white/80 dark:hover:bg-white"
              />
            </div>
          </div>
          <button
            onClick={handleOpenAdTaskForm}
            className="mb-3 rounded-lg border-2 border-yellow-300 px-3 py-1 font-semibold uppercase transition-colors duration-300 hover:border-transparent hover:bg-yellow-300 hover:text-white dark:border-transparent dark:bg-yellow-300 dark:text-black/80 dark:hover:bg-yellow-500 dark:hover:text-black"
          >
            Add
          </button>
          <hr />

          <Tab.Group>
            <Tab.List className="mx-auto flex w-fit flex-wrap gap-2 rounded-xl py-2 backdrop-blur-md md:gap-3 lg:mx-0">
              {onRenderTabHead()}
            </Tab.List>
            <Tab.Panels className="mx-3">{onRenderTabPanels()}</Tab.Panels>
          </Tab.Group>
        </div>
      </div>
    </div>
  );
}
