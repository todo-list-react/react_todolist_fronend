import React, { useEffect, useState } from "react";
import { useMediaQuery, useTheme } from "@mui/material";
import TodoListHeader from "./TodoListHeader";
import TodoListSideBar from "./TodoListSideBar";

interface Props {
  children: React.ReactNode;
}

export function TodoListLayout({ children }: Props) {
  const theme = useTheme();
  const isMoreThanMd = useMediaQuery(theme.breakpoints.up("md"));
  const [isSideBarOpen, setIsSideBarOpen] = useState(false);

  useEffect(() => {
    if (isMoreThanMd) {
      setIsSideBarOpen(true);
    } else {
      setIsSideBarOpen(false);
    }
  }, [isMoreThanMd]);
  return (
    <div className="relative min-h-screen">
      <TodoListHeader
        isSideBarOpen={isSideBarOpen}
        setIsSideBarOpen={setIsSideBarOpen}
      />
      <div className="flex">
        <TodoListSideBar
          isSideBarOpen={isSideBarOpen}
          isMoreThanMd={isMoreThanMd}
        />
        <div className="w-full">{children}</div>
      </div>
    </div>
  );
}
