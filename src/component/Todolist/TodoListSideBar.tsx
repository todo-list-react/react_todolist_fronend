import React, { useState } from "react";
import { DarkLightSwitcher } from "../DarkLightSwitcher/DarkLightSwitcher";
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import { taskService } from "../../services/taskService";
import { useTaskData } from "../../hooks/useTaskData";
import { IInitialTask } from "../../types/allTypes";
import moment from "moment";
import { RiUnpinFill } from "react-icons/ri";
import UpdateTaskForm from "../UpdateTaskForm/UpdateTaskForm";

interface Props {
  isSideBarOpen: boolean;
  isMoreThanMd: boolean;
}

function TodoListSideBar({ isMoreThanMd, isSideBarOpen }: Readonly<Props>) {
  const { data: taskData } = useTaskData();
  const queryClient = useQueryClient();
  const [updateTaskFormState, setUpdateTaskFormState] = useState({
    isOpen: false,
    taskId: "",
  });
  const unpinTask = useMutation(
    (taskId: string) => {
      return taskService.unpinTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully unpin task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to unpin task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const deleteTask = useMutation(
    (taskId: string) => {
      return taskService.deleteSingleTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully deleted task from List", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to remove task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const doneTask = useMutation(
    (taskId: string) => {
      return taskService.doneTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully complete task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to complete task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const renderTaskPinned = () => {
    return (taskData?.taskList as IInitialTask[])
      ?.filter((task) => task.pinned)
      .map((task, index) => {
        const handleOpenUpdateTaskForm = () => {
          setUpdateTaskFormState({
            ...updateTaskFormState,
            isOpen: true,
            taskId: task._id,
          });
        };

        const handleCloseUpdateTaskForm = () => {
          setUpdateTaskFormState({ ...updateTaskFormState, isOpen: false });
        };
        return (
          <div key={index.toString() + task._id}>
            {updateTaskFormState.isOpen &&
            updateTaskFormState.taskId === task._id ? (
              <UpdateTaskForm
                taskId={task._id}
                handleCloseUpdateTaskForm={handleCloseUpdateTaskForm}
              />
            ) : (
              <></>
            )}
            <div
              className={`${task.backgroundColor} ${task.textColor} w-full rounded-xl border border-slate-200 p-2 dark:border-transparent`}
            >
              <div className="flex justify-between gap-3">
                <div
                  title={task.taskName}
                  style={{ verticalAlign: "middle" }}
                  className="flex flex-col"
                >
                  <p>
                    {moment
                      .utc(new Date(task?.deadlineDate))
                      .format("DD/MM/YYYY HH:mm")}
                  </p>
                  {task.taskName}
                </div>
                <div className="text-right">
                  <button
                    onClick={() => {
                      unpinTask.mutate(task._id);
                    }}
                    className="rounded-md"
                  >
                    <RiUnpinFill />
                  </button>
                </div>
              </div>
              <div className="flex gap-2">
                <button
                  onClick={handleOpenUpdateTaskForm}
                  className="ml-1 border-0 "
                >
                  <i className="fa-solid fa-edit"></i>
                </button>
                <button
                  onClick={() => {
                    doneTask.mutate(task._id);
                  }}
                  className="ml-1 border-0"
                >
                  <i className="fa-solid fa-check"></i>
                </button>
                <button
                  onClick={() => {
                    deleteTask.mutate(task._id);
                  }}
                  className="ml-1 border-0"
                >
                  <i className="fa-solid fa-trash"></i>
                </button>
              </div>
            </div>
          </div>
        );
      });
  };
  return (
    <nav
      id="sidebar"
      className={`${
        !isSideBarOpen
          ? "w-0"
          : isMoreThanMd
            ? "w-1/3 xl:w-1/5"
            : "fixed z-10 w-full"
      } flex min-h-screen flex-col overflow-hidden whitespace-nowrap bg-slate-100 py-3 transition-all duration-300 dark:bg-slate-800`}
    >
      <div className="z-20 mx-auto pt-14">
        <DarkLightSwitcher />
      </div>

      <div className="px-3 py-5">
        <h2 className="text-2xl font-medium dark:text-yellow-300">
          Pinned Tasks
        </h2>
        <div className="flex flex-col gap-3">{renderTaskPinned()}</div>
      </div>
    </nav>
  );
}

export default TodoListSideBar;
