import React from "react";
import { useMutation, useQueryClient } from "react-query";
import { taskService } from "../../services/taskService";
import { toast } from "react-toastify";
import { useTaskData } from "../../hooks/useTaskData";
import { IInitialTask } from "../../types/allTypes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRotateBack,
  faStopwatch,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import moment from "moment";

function TaskCompleteComponent() {
  const { data: taskData } = useTaskData();
  const queryClient = useQueryClient();
  const deleteTask = useMutation(
    (taskId: string) => {
      return taskService.deleteSingleTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully deleted task from List", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to remove task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  const undoneTask = useMutation(
    (taskId: string) => {
      return taskService.undoneTask(taskId);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully reverse task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to reverse task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );
  const renderTaskComplete = () => {
    return (taskData?.taskList as IInitialTask[])
      ?.filter((task) => task.done)
      .map((task, index) => {
        return (
          <div key={index.toString() + task._id}>
            <div
              className={`${task.backgroundColor} ${task.textColor} w-fit rounded-xl p-4 dark:text-white dark:saturate-150`}
            >
              <div className="flex items-center justify-between gap-5 pb-3">
                <div>
                  {moment
                    .utc(new Date(task?.deadlineDate))
                    .format("DD/MM/YYYY HH:mm")}
                </div>
                <div className="flex flex-wrap gap-2">
                  <button
                    onClick={() => {
                      undoneTask.mutate(task._id);
                    }}
                    className="ml-1 border-0"
                  >
                    <FontAwesomeIcon icon={faArrowRotateBack} />
                  </button>
                  <button
                    onClick={() => {
                      deleteTask.mutate(task._id);
                    }}
                    className="ml-1 border-0"
                  >
                    <FontAwesomeIcon icon={faTrash} />
                  </button>
                </div>
              </div>
              <div title={task.taskName} className="text-5xl">
                {task.taskName}
              </div>
            </div>
          </div>
        );
      });
  };

  return (
    <div>
      <div className="flex flex-wrap gap-6">{renderTaskComplete()}</div>
    </div>
  );
}

export default TaskCompleteComponent;
