import React, { useState } from "react";
import "react-calendar/dist/Calendar.css";
import Calendar from "react-calendar";

type ValuePiece = Date | null;

type Value = ValuePiece | [ValuePiece, ValuePiece];

export default function CalendarComponent() {
  const [date, setDate] = useState<Value>(new Date());
  return (
    <div>
      <h2>Calendar</h2>
      <div className="calendar-container">
        <Calendar onChange={setDate} value={date} minDetail="year" />
      </div>
      <p className="text-center">
        <span className="bold">Selected Date:</span> {date?.toString()}
      </p>
    </div>
  );
}
