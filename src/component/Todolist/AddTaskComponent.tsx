import React, { useEffect, useState } from "react";
import { useAppSelector, useAppDispatch } from "../../hooks/hooks";
import { updateTask } from "../../redux/slices/taskSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileImport, faPlus } from "@fortawesome/free-solid-svg-icons";
import { randomColorList } from "../../utils/randomColorList";
import { useMutation, useQueryClient } from "react-query";
import { toast } from "react-toastify";
import { taskService } from "../../services/taskService";
import { IAddTaskRequestBody } from "../../types/allTypes";
import { initialQueryTask } from "../../utils/utils";

function AddTaskComponent() {
  const [inputValue, setInputValue] = useState("");
  const selectedTask = useAppSelector((state) => state.taskSlice.selectTask);
  const dispatch = useAppDispatch();
  const isEdit = !!selectedTask?._id;
  const isAddTask = !selectedTask?._id;

  const randomColor = randomColorList[Math.floor(Math.random() * 5)];

  const queryClient = useQueryClient();
  const addTask = useMutation(
    (requestBody: IAddTaskRequestBody) => {
      return taskService.addTask(requestBody);
    },
    {
      onSuccess: (success) => {
        queryClient.invalidateQueries("taskData");
        toast.success("Successfully deleted task from List", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
      onError: () => {
        toast.error("Failed to remove task", {
          position: toast.POSITION.TOP_CENTER,
        });
      },
    }
  );

  useEffect(() => {
    setInputValue(selectedTask.taskName);
  }, [selectedTask]);

  const onRenderTaskAction = () => {
    const actionArr = [
      {
        id: 1,
        title: "Add task",
        icon: <FontAwesomeIcon icon={faPlus} />,
        feature: () => {
          addTask.mutate({
            ...initialQueryTask,
            taskName: inputValue,
            backgroundColor: randomColor.background,
            textColor: randomColor.text,
          });
          setInputValue("");
        },
        disableCondition: isEdit,
      },
      {
        id: 2,
        title: "Update task",
        icon: <FontAwesomeIcon icon={faFileImport} />,
        feature: () => {
          const updatedTask = {
            ...selectedTask,
            taskName: inputValue,
          };
          dispatch(updateTask(updatedTask));
          setInputValue("");
        },
        disableCondition: isAddTask,
      },
    ];

    return actionArr.map((item) => {
      return (
        <button
          key={item.id}
          disabled={item.disableCondition}
          onClick={item?.feature}
          className="rounded-[70%_30%_30%_70%/_60%_40%_60%_40%] border-2 border-yellow-400 p-3 transition-colors duration-300 hover:bg-yellow-400 hover:text-white disabled:cursor-not-allowed"
          title={item.title}
        >
          {item.icon}
        </button>
      );
    });
  };

  return (
    <div className="flex gap-3 pb-5">
      <div className="flex items-center gap-2">
        <label className="d-block">Task name</label>
        <input
          onKeyDown={(e) => {
            if (e.key === "Enter" && isAddTask) {
              addTask.mutate({
                ...initialQueryTask,
                taskName: inputValue,
                backgroundColor: randomColor.background,
                textColor: randomColor.text,
              });
              setInputValue("");
            }
          }}
          onChange={(e) => {
            setInputValue(e.target.value);
          }}
          value={inputValue}
          id="input_task"
          className="w-50 rounded-lg border p-1"
          type="text"
        />
      </div>
      <div className="flex gap-2">{onRenderTaskAction()}</div>
    </div>
  );
}

export default AddTaskComponent;
