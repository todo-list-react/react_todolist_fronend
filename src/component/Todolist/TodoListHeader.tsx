import React, { Dispatch, SetStateAction } from "react";

interface Props {
  isSideBarOpen: boolean;
  setIsSideBarOpen: Dispatch<SetStateAction<boolean>>;
}

function TodoListHeader({ isSideBarOpen, setIsSideBarOpen }: Readonly<Props>) {
  return (
    <nav className="absolute z-20 w-screen">
      <div className="mx-auto px-3">
        <div className="relative flex h-16 items-center justify-between">
          <div className="absolute inset-y-0 left-0 flex items-center md:static ">
            <button
              type="button"
              className="inline-flex items-center justify-center rounded-md border border-transparent p-2 text-gray-400 hover:text-white focus:outline-none focus:ring-0 "
              aria-expanded="false"
            >
              <div className="w-5">
                <input
                  onClick={() => {
                    setIsSideBarOpen(!isSideBarOpen);
                  }}
                  type="checkbox"
                  id="menu"
                  className="hidden"
                />
                <label
                  htmlFor="menu"
                  className="icon block h-full w-full cursor-pointer py-3"
                >
                  <div className="menu hover:bg-rd-flashturq hover:before:bg-rd-flashturq hover:after:bg-rd-flashturq before:-top-[0.4rem] after:top-[0.4rem]" />
                </label>
              </div>
            </button>
          </div>
          <div className="flex flex-1 items-center justify-center sm:items-stretch md:ml-3 md:justify-start">
            <div className="flex flex-shrink-0 items-center">
              <a
                href={"/"}
                className="flex items-center gap-2 text-2xl font-bold text-black dark:text-white"
              >
                <img
                  src="https://cdn2.iconfinder.com/data/icons/metro-uinvert-dock/256/Google_Tasks.png"
                  alt="Rapiddweller White Logo"
                  className="w-10"
                />
                UPtask
              </a>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default TodoListHeader;
