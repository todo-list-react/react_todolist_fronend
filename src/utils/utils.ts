export const initialTask = {
  _id: "",
  taskName: "",
  done: false,
  pinned: false,
  deadlineDate: new Date(),
  backgroundColor: "bg-orange-300/80",
  textColor: "text-slate-700",
};

export const initialQueryTask = {
  taskName: "",
  done: false,
  pinned: false,
  deadlineDate: new Date(),
  backgroundColor: "bg-orange-300/80",
  textColor: "text-slate-700",
};
