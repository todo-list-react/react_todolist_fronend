export const randomColorList = [
  {
    background: "bg-orange-300/80",
    text: "text-slate-700",
  },
  {
    background: "bg-red-300/80",
    text: "text-slate-700",
  },
  {
    background: "bg-blue-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-yellow-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-lime-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-emerald-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-cyan-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-indigo-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-violet-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-rose-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-purple-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-pink-200/80",
    text: "text-slate-700",
  },
  {
    background: "bg-slate-700/80",
    text: "text-white",
  },
];
