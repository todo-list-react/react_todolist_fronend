import type { Dispatch, SetStateAction } from "react";
import { createContext } from "react";

interface AppContextProps {
  isDarkMode?: boolean;
  setIsDarkMode?: Dispatch<SetStateAction<boolean>>;
}

export const AppContext = createContext<AppContextProps>({});
