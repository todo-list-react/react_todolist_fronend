import mongoose from "mongoose";
mongoose.Promise = global.Promise;

const taskSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  taskName: {
    type: String,
    required: true,
  },
  done: {
    type: Boolean,
    required: true,
  },
  pinned: {
    type: Boolean,
    required: true,
  },
  deadlineDate: {
    type: Date,
    required: true,
  },
  backgroundColor: {
    type: String,
    required: true,
  },
  textColor: {
    type: String,
    required: true,
  },
});

export default mongoose.model("Task", taskSchema);
