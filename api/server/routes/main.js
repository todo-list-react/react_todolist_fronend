import taskAction from "../controllers/tasks.js";

const mainRoutes = (app) => {
  app.post("/api/tasks/create", taskAction.createTask);
  app.get("/api/tasks", taskAction.getAllTask);
  app.get("/api/tasks/:taskId", taskAction.getSingleTask);
  app.delete("/api/tasks/:taskId", taskAction.deleteTask);
  app.put("/api/tasks/:taskId/pinned", taskAction.managePinTask);
  app.put("/api/tasks/:taskId/done", taskAction.manageDoneTask);
  app.put("/api/tasks/:taskId/update", taskAction.updateTask);
};

export default mainRoutes;
