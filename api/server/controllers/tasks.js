import mongoose from "mongoose";
import Task from "../models/task.js";

// create new cause
function createTask(req, res) {
  const task = new Task({
    _id: new mongoose.Types.ObjectId(),
    taskName: req.body.taskName,
    done: req.body.done,
    pinned: req.body.pinned,
    deadlineDate: req.body.deadlineDate,
    backgroundColor: req.body.backgroundColor,
    textColor: req.body.textColor,
  });

  return task
    .save()
    .then((newTask) => {
      return res.status(201).json({
        success: true,
        message: "New cause created successfully",
        task: newTask,
      });
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
        error: error.message,
      });
    });
}

const getAllTask = (req, res) => {
  Task.find()
    .select("_id taskName done pinned deadlineDate backgroundColor textColor")
    .then(async (allTask) => {
      return await res.status(200).json({
        success: true,
        message: "A list of all task",
        taskList: allTask,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
        error: err.message,
      });
    });
};

const getSingleTask = (req, res) => {
  const id = req.params.taskId;
  Task.findById(id)
    .then((singleTask) => {
      res.status(200).json({
        success: true,
        message: `More on ${singleTask?.taskName}`,
        task: singleTask,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "This course does not exist",
        error: err.message,
      });
    });
};

const deleteTask = (req, res) => {
  const id = req.params.taskId;
  Task.findByIdAndDelete(id)
    .exec()
    .then(() =>
      res.status(204).json({
        success: true,
      })
    )
    .catch((err) =>
      res.status(500).json({
        success: false,
      })
    );
};

const managePinTask = (req, res) => {
  const id = req.params.taskId;

  Task.findByIdAndUpdate(id, {
    pinned: !!(req.query.pinned && req.query.pinned === "true"),
  })
    .then((results) => {
      res.status(200).json({
        success: true,
      });
    })
    .catch((err) => {
      console.log("err: ", err);
      res.status(500).json({
        success: false,
      });
    });
};

const manageDoneTask = (req, res) => {
  const id = req.params.taskId;

  Task.findByIdAndUpdate(id, {
    done: !!(req.query.done && req.query.done === "true"),
  })
    .then((results) => {
      Task.findByIdAndUpdate(id, {
        pinned: false,
      })
        .then((results) => {
          res.status(200).json({
            success: true,
          });
        })
        .catch((err) => {
          console.log("err: ", err);
          res.status(500).json({
            success: false,
          });
        });
    })
    .catch((err) => {
      console.log("err: ", err);
      res.status(500).json({
        success: false,
      });
    });
};

export function updateTask(req, res) {
  const id = req.params.taskId;
  const updateObject = { ...req.body, _id: id };
  Task.updateOne({ _id: id }, { $set: updateObject })
    .then(() => {
      res.status(200).json({
        success: true,
        message: "Task is updated",
        updatedTask: updateObject,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
      });
    });
}

const taskAction = {
  createTask,
  getAllTask,
  getSingleTask,
  deleteTask,
  managePinTask,
  manageDoneTask,
  updateTask,
};

export default taskAction;
